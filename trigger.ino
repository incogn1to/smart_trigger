#include <IRremote.h>

//the time we give the sensor to calibrate (10-60 secs according to the datasheet)    
const int calibrationTime = 20;        
 
//the time when the sensor outputs a low impulse
long unsigned int lowIn;        
 
//the amount of milliseconds the sensor has to be low
//before we assume all motion has stopped
long unsigned int pause = 5000; 
 
boolean lockLow = true;
boolean takeLowTime;

 
const int pirPin = 13;    //the digital pin connected to the PIR sensor's output
const int remotePin = 14; // pin for IR remote 
const int detonationPin = 15; // pin is triggered on detionation
const int motionLed = 2; // led is active when motion detector seens something
const int armedLed = 16; // led shows that unit is active
//const int armedPin = 17; // pin used to arm unit

// pins used for counter
const int cnt_pos_1 = 12;
const int cnt_pos_2 = 11;
const int cnt_pos_3 = 10;
const int cnt_pos_4 = 9;
const int cnt_pos_5 = 8; 

// pins used for timer
const int time_pos_1 = 7;
const int time_pos_2 = 6;
const int time_pos_3 = 5;
const int time_pos_4 = 4;
const int time_pos_5 = 3; 

bool armed = false; // if unit is armed or not
//int pirState = LOW; 
//int val = 0;

// PIR variables
int state = LOW;             // by default, no motion detected
int val = 0;                 // variable to store the sensor status (value)

IRrecv irrecv(remotePin);
decode_results results;
unsigned long key_value = 0; // last IR cmd value
 
/////////////////////////////
//SETUP
void setup(){
  Serial.begin(9600);

  pinMode(pirPin, INPUT);
  pinMode(remotePin, INPUT);


  pinMode(motionLed, OUTPUT);
  pinMode(armedLed, OUTPUT);
  pinMode(detonationPin, OUTPUT);

  // pins used for counter position
  pinMode(cnt_pos_1, INPUT_PULLUP);
  pinMode(cnt_pos_2, INPUT_PULLUP);
  pinMode(cnt_pos_3, INPUT_PULLUP);
  pinMode(cnt_pos_4, INPUT_PULLUP);
  pinMode(cnt_pos_5, INPUT_PULLUP);

  // pins used for counter position
  pinMode(time_pos_1, INPUT_PULLUP);
  pinMode(time_pos_2, INPUT_PULLUP);
  pinMode(time_pos_3, INPUT_PULLUP);
  pinMode(time_pos_4, INPUT_PULLUP);
  pinMode(time_pos_5, INPUT_PULLUP);    

  //digitalWrite(armedPin, LOW);      
  digitalWrite(pirPin, LOW);
  digitalWrite(detonationPin, LOW);
  digitalWrite(remotePin, LOW);
  digitalWrite(armedLed, LOW);
 
  //give the sensor some time to calibrate
  Serial.print("calibrating sensor ");
    for(int i = 0; i < calibrationTime; i++){
      Serial.print(".");
      delay(1000);
      }
    Serial.println(" done");
    Serial.println("SENSOR ACTIVE");
    delay(50);

    
    irrecv.enableIRIn();
    irrecv.blink13(true);
                        
    Serial.println("BOOT completed");
}

// returns requested work time in seconds
long get_work_time(int pos)
{
    return pos * 60; // from 1 to 5 minutes. used for testing
    /*
    switch(pos)
    {
      case 1: return 10 * 60; // 10 min
      case 2: return 30 * 60; // 30 min
      case 3: return 60 * 60; // 1 hour
      case 4: return 120 * 60; // 2 hour
      case 6: return 6 * 60 * 60; // 6 hours
    }
    */
} 

// returns number of car should be blown
int get_counter_position()
{
  //return 5; // used for testing
  int cnt = 0;
  
  if (digitalRead(cnt_pos_5) == LOW)
  {
    cnt = 5;
  }
  else if (digitalRead(cnt_pos_4) == LOW)
  {
    cnt = 4;
  }  
  else if (digitalRead(cnt_pos_3) == LOW)
  {
    cnt = 3;
  } 
  else if (digitalRead(cnt_pos_2) == LOW)
  {
    cnt = 2;
  }
  else if (digitalRead(cnt_pos_1) == LOW)
  {
    cnt = 1;
  }

 
  return cnt;
}

void read_cmd()
{
  if (irrecv.decode(&results))
  {
        if (results.value == 0XFFFFFFFF)
          results.value = key_value;
          
        switch(results.value){
          case 0xFF30CF:
            Serial.println("Command: Armed (1)");
            armed = true;
            break ;
          case 0xFF18E7:
            Serial.println("Command: Blow (2)");
            detonation();
            break ;
          default:
            Serial.println("Default"); 
            // just to be sure
            if (armed)
            {
              detonation();
            }
            else
            {
              armed = true;
            }
          /*          
          case 0xFFA25D:
          Serial.println("CH-");
          break;
          case 0xFF629D:
          Serial.println("CH");
          break;
          case 0xFFE21D:
          Serial.println("CH+");
          break;
          case 0xFF22DD:
          Serial.println("|<<");
          break;
          case 0xFF02FD:
          Serial.println(">>|");
          break ;  
          case 0xFFC23D:
          Serial.println(">|");
          break ;               
          case 0xFFE01F:
          Serial.println("-");
          break ;  
          case 0xFFA857:
          Serial.println("+");
          break ;  
          case 0xFF906F:
          Serial.println("EQ");
          break ;  
          case 0xFF6897:
          Serial.println("0");
          break ;  
          case 0xFF9867:
          Serial.println("100+");
          break ;
          case 0xFFB04F:
          Serial.println("200+");
          break ;
          
          case 0xFF7A85:
          Serial.println("3");
          break ;
          case 0xFF10EF:
          Serial.println("4");
          break ;
          case 0xFF38C7:
          Serial.println("5");
          break ;
          case 0xFF5AA5:
          Serial.println("6");
          break ;
          case 0xFF42BD:
          Serial.println("7");
          break ;
          case 0xFF4AB5:
          Serial.println("8");
          break ;
          case 0xFF52AD:
          Serial.println("9");
          break ;   
          */   
        }
        key_value = results.value;
        irrecv.resume(); 
  }
}

// returns number of car should be blown
int get_timer_position()
{
  //return 1; // used for testing
  int cnt = 0;
  
  if (digitalRead(time_pos_5) == LOW)
  {
    cnt = 5;
  }
  else if (digitalRead(time_pos_4) == LOW)
  {
    cnt = 4;
  }  
  else if (digitalRead(time_pos_3) == LOW)
  {
    cnt = 3;
  } 
  else if (digitalRead(time_pos_2) == LOW)
  {
    cnt = 2;
  }
  else if (digitalRead(time_pos_1) == LOW)
  {
    cnt = 1;
  }

  return cnt;
}

bool check_for_vechicle()
{ 
  bool detected = false;
  val = digitalRead(pirPin);   // read sensor value
  if (val == HIGH) {           // check if the sensor is HIGH
    //digitalWrite(led, HIGH);   // turn LED ON
    delay(100);                // delay 100 milliseconds 
    
    if (state == LOW) {
      Serial.println("Motion detected!"); 
      state = HIGH;       // update variable state to HIGH
      detected = true;
    }
  } 
  else {
      //digitalWrite(led, LOW); // turn LED OFF
      delay(200);             // delay 200 milliseconds 
      
      if (state == HIGH){
        Serial.println("Motion stopped!");
        state = LOW;       // update variable state to LOW
    }
  }

  return detected;
}

void detonation_pin( bool active )
{
  if (active)
  {
      digitalWrite(detonationPin, HIGH); 
      Serial.println("!!!boom!!!");
  }
  else
  {
      digitalWrite(detonationPin, LOW);
  }
}

void armed_led( bool active )
{
  if (active)
  {
      digitalWrite(armedLed, HIGH); 
  }
  else
  {
      digitalWrite(armedLed, LOW);; 
  }
}

void motion_led( bool active )
{
  if (active == true)
  {
      digitalWrite(motionLed, HIGH); 
  }
  else
  {
      digitalWrite(motionLed, LOW);
      //digitalWrite(motionLed, HIGH);
  }
}

void detonation()
{
  if (armed)
  {
    armed = false;
    armed_led( false );
    //motion_led( false );
    detonation_pin( true );  // boom
    delay(1000);
  }
  else
  {
    Serial.println("unit is not armed");
  }
}
 
////////////////////////////

void loop()
{
    Serial.println("Unit is not armed!");
    detonation_pin( false );
    armed_led( false );
    motion_led( true );
    read_cmd();
    
    while (armed)
    {
        Serial.println("Unit is armed!");
        
        int counter = get_counter_position();
        int timer_pos = get_timer_position();
        Serial.print("counter position:");
        Serial.println(counter);
        Serial.print("timer position:");
        Serial.println(timer_pos);

 
        if ( counter <= 0 || timer_pos <= 0 )
        {
          Serial.println("Unit is not configured properly");
          armed = false;
          armed_led( true );
          delay(300);
          armed_led( false );
          delay(300);
          armed_led( true );
          delay(300);
          armed_led( false );
          continue;
        }

        armed_led( true );
        long work_time = get_work_time( timer_pos );
        Serial.print("Work time(sec):");
        Serial.println(work_time);
        Serial.print("Selected vechicle number:");
        Serial.println(counter);
           
        long endtime = millis() + 1000 * work_time;

        while( armed )
        {           
            bool vechicle_detected = check_for_vechicle();
            if (vechicle_detected)
            {    
                counter--;
                Serial.print("Vechicle detected. Remaining vechicles:");
                Serial.println(counter);
            }
            else
            {
                Serial.println("Nothing is detected"); 
            }
            
            if ( counter <= 0 )
            {
                detonation();
                break;
            }

            bool expired = millis() >= endtime;
            Serial.print("endtime:");
            Serial.print(endtime);
            Serial.print(" current time:");
            Serial.println(millis());
            if ( expired )
            {
              Serial.println("working time have expired. Deactivating");
              armed = false;
              armed_led( false );
              //motion_led( false );
              continue;
            }

            read_cmd();
            delay(200);
        }
    } 
    delay(200);   
}  
